import * as commander from 'commander';
import * as inquirer from 'inquirer';

import { points } from './inputQuestions';

commander.version('1.0.0').description('Testing');

commander
  .command('points')
  .alias('p')
  .description('Getting cartesian Points')
  .action(() => {
    inquirer.prompt(points).then(answers => {
      arrayFromPoints((<any>answers).points);
    });
  });

commander.parse(process.argv);

function arrayFromPoints(points: string) {
  // TODO: Assignment logic goes here
}
