# TS Assignments

Clone this repo to get start with Typescript Assignments

## Installation


```console
npm install -g ts-node
npm install -g typescript
```

After that, run the following command to install the dependencies

```console
npm install
```

## Usage


```console
# Execute code with TypeScript

ts-node main.ts points <points>
```

you can get points from the points.txt file